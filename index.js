function onOpen() {
  var ui = SpreadsheetApp.getUi();
  // Or DocumentApp or FormApp.
  ui.createMenu('Sheet Cart')
      .addItem('Generate Template', 'generateTemplate')
      .addSeparator()
      .addItem('Publish Products', 'publishProducts')
      .addSeparator()
      .addItem('Admin', 'navigateToAdmin')
      .addToUi();
}

function getCredentialsSheetData(sheet) {

  // This represents ALL the data
  var range = sheet.getDataRange();
  var values = range.getValues();

  const sheetCartKey = values[0][1]
  const accountId = values[1][1]
  const stripeKey = values[2][1]

  return {
    sheetCartKey: sheetCartKey,
    stripeKey: stripeKey,
    accountId: accountId
  }
}

function updateProductsIds(sheet, products) {

  var range = sheet.getDataRange()
  var values = range.getValues()

  for (var i = 0; i < values.length; i++) {

    // skip head of document
    if (i === 0) {

      continue
    }

    for (var j = 0; j < values[i].length; j++) {

      var cellValue = values[i][j]

      if (j === 4 && cellValue === '') {

        values[i][j] = products[i - 1].productId
      }

      if (j === 5 && cellValue === '') {

        values[i][j] = products[i - 1].skuId
      }
    }
  }

  range.setValues(values)
}

function getProductsSheetData(sheet) {

  var range = sheet.getDataRange();
  var values = range.getValues();

  var products = []

  for (var i = 0; i < values.length; i++) {

    // skip head of document
    if (i === 0) {

      continue
    }

    var product = {}

    for (var j = 0; j < values[i].length; j++) {

      var cellValue = values[i][j]

      if (j === 0 && cellValue) {

        product.name = cellValue
      }

      if (j === 1 && cellValue) {

        product.price = cellValue
      }

      if (j === 2 && cellValue) {

        product.description = cellValue
      }

      if (j === 3 && cellValue) {

        product.image = cellValue
      }

      if (j === 4 && cellValue) {

        product.productId = cellValue
      }

      if (j === 5 && cellValue) {

        product.skuId = cellValue
      }
    }

    products.push(product)
  }

  return products
}

function publishProducts() {

  const activeSpreadsheet = SpreadsheetApp.getActiveSpreadsheet()
  const productsSpreadsheet = activeSpreadsheet.getSheetByName('Products')
  const credentialsSpreadsheet = activeSpreadsheet.getSheetByName('Credentials')

  const products = getProductsSheetData(productsSpreadsheet)
  const credentials = getCredentialsSheetData(credentialsSpreadsheet)

  var data = {
    'productList': products
  }

  var options = {
    'method' : 'post',
    'contentType': 'application/json',
    'payload' : JSON.stringify(data)
  }

  const productsPostUrl = 'https://gmijyxi2m3.execute-api.us-east-1.amazonaws.com/test/products/' + credentials.accountId

  const response = UrlFetchApp.fetch(productsPostUrl, options)

  const responseObject = JSON.parse(response)

  updateProductsIds(productsSpreadsheet, responseObject.products)

  SpreadsheetApp.getUi()
     .alert('New products published!');
}

function generateTemplate() {

  var ui = SpreadsheetApp.getUi()

  var response = ui.prompt('Generate Template', 'Re-generate template, clearing all existing data', ui.ButtonSet.YES_NO);

  // Process the user's response.
  if (response.getSelectedButton() == ui.Button.YES) {

    const activeSpreadsheet = SpreadsheetApp.getActiveSpreadsheet()

    const productsSpreadsheet = activeSpreadsheet.getSheetByName('Products')
    const credentialsSpreadsheet = activeSpreadsheet.getSheetByName('Credentials')

    productsSpreadsheet.clear()
    credentialsSpreadsheet.clear()

    productsSpreadsheet.appendRow(['Name', 'Price', 'Description', 'Image', 'Product ID', 'SKU ID'])
    credentialsSpreadsheet.appendRow(["Sheet Cart Key", ""])
    credentialsSpreadsheet.appendRow(["Account Id", ""])
    credentialsSpreadsheet.appendRow(["Stripe Key", ""])

    SpreadsheetApp.getUi()
      .alert('Generate sheet template');
  } else if (response.getSelectedButton() == ui.Button.NO) {

    // clicked no
  } else {

    // clicked close
  }
}

function navigateToAdmin() {

  var js = " \
    <script> \
      window.open('https://google.com/', '_blank', 'width=800, height=600'); \
      google.script.host.close(); \
    </script> \
  ";
  var html = HtmlService.createHtmlOutput(js)
    .setHeight(10)
    .setWidth(100);
  SpreadsheetApp.getUi().showModalDialog(html, 'Now loading.');
}
